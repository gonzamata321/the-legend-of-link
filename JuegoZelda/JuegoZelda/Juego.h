#include <SFML/Graphics.hpp>
#include "Zelda.h"
#include "Enemigo.h"
#include "Scene.h"

class Juego
{
private:
	int mapX;
	int mapY;
	bool dead = false;
public:
	Juego();
	Juego(int x, int y);
	~Juego();
	void Run();
};

