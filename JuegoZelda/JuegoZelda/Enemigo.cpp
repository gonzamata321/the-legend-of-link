#include "Enemigo.h"



Enemigo::Enemigo(double x, double y, bool V, double vv)
{
	eX = x;
	eY = y;
	vertical = V;
	velocidad = vv;
}

Enemigo::~Enemigo()
{

}
bool Enemigo::getVertical()
{
	return vertical;
}
double Enemigo::getX()
{
	return eX;
}
double Enemigo::getY()
{
	return eY;
}
float Enemigo::getWhidth()
{
	return sprite.getLocalBounds().width;
}
float Enemigo::getHeight()
{
	return sprite.getLocalBounds().height;
}



void Enemigo::Update(int max)
{
	if (vertical == true)
	{
		if (eX < max && cambio == true)
		{
			eX = eX + (1 * velocidad);
		}
		else
		{
			cambio = false;
			eX = eX - (1 * velocidad);
			if(eX <= 0)
			{
				cambio = true;
			}
		}
	}
	if (vertical == false)
	{
		if (eY < max && cambio == true)
		{
			eY = eY + (1 * velocidad);
		}
		else
		{
			cambio = false;
			eY = eY - (1 * velocidad);
			if (eY <= 0)
			{
				cambio = true;
			}
		}
	}
}

sf::Sprite Enemigo::Draw()
{
	sf::Texture texture;
	texture.loadFromFile("../png/Enemigo.jpg");

	sprite.setTexture(texture);
	sprite.setPosition(eX, eY);

	return sprite;
}