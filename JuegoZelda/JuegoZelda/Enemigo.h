#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>
class Enemigo
{
private:
	bool vivo = true;
	bool vertical;
	bool cambio = true;
	double eX;
	double eY;
	double velocidad;
	sf::Sprite sprite;
public:
	Enemigo(double x, double y,bool V, double velocidad);
	~Enemigo();
	void Update(int max);
	bool getVertical();
	double getX();
	double getY();
	float getWhidth();
	float getHeight();
	sf::Sprite Draw();
};

