#include "Zelda.h"



Zelda::Zelda(double x, double y)
{
	xZ = x;
	yZ = y;
	height = 20;
	width = 20;
	velocidad = 0.1;
}


Zelda::~Zelda()
{
}

double Zelda::getX()
{
	return xZ;
}
double Zelda::getY()
{
	return yZ;
}
float Zelda::getWhidth()
{
	return sprite.getLocalBounds().width;
}
float Zelda::getHeight()
{
	return sprite.getLocalBounds().height;
}

void Zelda::Update()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		xZ = xZ + (1 * velocidad);
		std::cout << "DERECHA" << std::endl;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		xZ = xZ - (1 * velocidad);
		std::cout << "IZQUIERDA" << std::endl;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		yZ = yZ - (1 * velocidad);
		std::cout << "UP" << std::endl;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		yZ = yZ + (1 * velocidad);
		std::cout << "DOWN" << std::endl;
	}
}

sf::Sprite Zelda::Draw()
{
	sf::Texture texture;
	texture.loadFromFile("../png/Player.jpg");

	sprite.setTexture(texture);
	sprite.setPosition(xZ,yZ);

	return sprite;
}