#include "Juego.h"



Juego::Juego()
{
	mapX = 640;
	mapY = 480;
}

Juego::Juego(int x, int y)
{
	mapX = x;
	mapY = y;
}


Juego::~Juego()
{
}

void Juego::Run()
{
	sf::RenderWindow window(sf::VideoMode(mapX, mapY), "SFML works!");
	
	sf::Texture texture;
	texture.loadFromFile("../png/fondo.jpg");
	
	sf::Sprite sprite;
	sprite.setTexture(texture);
	sprite.setTextureRect(sf::IntRect(0, 0, mapX, mapY));
	
	Zelda * p = new Zelda(140, 140);
	Enemigo * e[20];
	Scene * s = new Scene();
	for (int a = 0; a < 20; a++)
	{
		e[a] = new Enemigo(40, 40, false, 0.1);
	}
	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		dead = s->colicion(p, e[1]);
		if (dead == true)
		{
			std::cout << "SIIII";
		}
		window.clear();
		p->Update();
		e[1]->Update(sprite.getLocalBounds().width);
		window.draw(sprite);
		window.draw(p->Draw());
		window.draw(e[1]->Draw());
		window.display();
	}
	
}
