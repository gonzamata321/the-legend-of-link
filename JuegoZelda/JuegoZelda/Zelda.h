#pragma once
#include <SFML/Graphics.hpp>
#include<iostream>

class Zelda
{
private:
		int height;
		int width;
		double xZ;
		double yZ;
		double velocidad;
		bool vivo = true;
		sf::Sprite sprite;
public:
	Zelda(double x, double y);
	~Zelda();
	void Update();
	double getX();
	double getY();
	float getWhidth();
	float getHeight();
	sf::Sprite Draw();
};

